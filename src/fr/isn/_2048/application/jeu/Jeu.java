package fr.isn._2048.application.jeu;

import fr.isn._2048.application.jeu.utile.Vecteur2D;
import fr.isn._2048.application.jeu.utile.pondere.PondereAleatoire;
import fr.isn._2048.application.jeu.utile.pondere.PondereObjet;

import java.util.*;

public class Jeu
{

    private int victoireTuileValeur;
    private short grilleNombreLigneColonne; //Variable de type short afin que le nombre de case (ligne * colonne) puisse être contenu dans un int.
    private int commencementPartieNombreTuileAjout;
    private int deplacementTuilesNombreTuileAjout;
    private PondereAleatoire pondereAleatoireTuileAjout;

    private List<JeuEvenement> jeuEvenements;

    private Integer[][] grilleTuiles; //Tableau représentant la grille afin d'y placer les tuiles.
    private List<Vecteur2D> grilleCoordonneesDisponibles;
    private Random aleatoireGrilleCoordonnees;
    private int partieScore;
    private PartieEtat partieEtat;

    public Jeu()
    {
        victoireTuileValeur = 2048;
        grilleNombreLigneColonne = 4;
        commencementPartieNombreTuileAjout = 2;
        deplacementTuilesNombreTuileAjout = 1;
        pondereAleatoireTuileAjout = new PondereAleatoire();
        pondereAleatoireTuileAjout.ajouterPondereObjet(new PondereObjet(90, 2)); //90% de chance que la tuile de valeur 2 soit ajoutée.
        pondereAleatoireTuileAjout.ajouterPondereObjet(new PondereObjet(10, 4)); //10% de chance que la tuile de valeur 4 soit ajoutée.

        jeuEvenements = new ArrayList<>();
    }

    public int victoireTuileValeur()
    {
        return victoireTuileValeur;
    }

    public short grilleNombreLigneColonne()
    {
        return grilleNombreLigneColonne;
    }

    public void ajouterJeuEvenement(JeuEvenement jeuEvenement)
    {
        if (jeuEvenement != null && !jeuEvenements.contains(jeuEvenement))
        {
            jeuEvenements.add(jeuEvenement);
        }
    }

    //Inutilisée
    public boolean contientJeuEvenement(JeuEvenement jeuEvenement)
    {
        return jeuEvenements.contains(jeuEvenement);
    }

    //Inutilisée
    public void retirerJeuEvenement(JeuEvenement jeuEvenement)
    {
        jeuEvenements.remove(jeuEvenement);
    }

    private Map<Vecteur2D, Integer> ajouterTuilePondereeAleatoireCoordonneesAleatoiresRequete(int nombre)
    {
        Map<Vecteur2D, Integer> nouvellesTuiles = new LinkedHashMap<>(); //Informations.

        for (int i = 0; i < nombre; i++)
        {
            if (grilleCoordonneesDisponibles.isEmpty()) //Invalidation de la requête.
            {
                //La requête est invalide.

                return nouvellesTuiles;
            }

            //La requête est valide. Au moins une case est disponible.

            //Récupération aléatoire d'une case disponible dans la grille et d'une valeur pondérée pour la tuile.
            int index = aleatoireGrilleCoordonnees.nextInt(grilleCoordonneesDisponibles.size());
            Vecteur2D grilleCoordonnees = grilleCoordonneesDisponibles.get(index);
            int tuileValeur = (Integer) pondereAleatoireTuileAjout.aleatoireObjet();

            grilleTuiles[grilleCoordonnees.y()][grilleCoordonnees.x()] = tuileValeur; //Ajout de la tuile dans la grille.
            grilleCoordonneesDisponibles.remove(index); //La case devient alors indisponible.

            nouvellesTuiles.put(grilleCoordonnees, tuileValeur); //Mise des informations.
        }

        return nouvellesTuiles;
    }

    /*
    Vecteur représentant la direction du déplacement.
    Pour la direction vers le haut, le vecteur a un sens négatif sur l'axe des ordonnées. C'est à dire de bas en haut sur une colonne de la grille.
    Pour la direction vers le bas, le vecteur a un sens positif sur l'axe des ordonnées. C'est à dire de haut en bas sur une colonne de la grille.
    Pour la direction vers la gauche, le vecteur a un sens négatif sur l'axe des abscisses. C'est à dire de droite à gauche sur une ligne de la grille.
    Pour la direction vers la droite, le vecteur a un sens positif sur l'axe des abscisses. C'est à dire de gauche à droite sur une ligne de la grille.
    La tuile représentant l'origine de la grille est celle située à la ligne la plus en haut et à la colonne la plus à gauche.
    La tuile la plus loin de l'origine de la grille est donc celle située à la ligne la plus en bas et à la colonne la plus à droite.
     */
    private Vecteur2D deplacementDirectionVecteur(DeplacementDirection deplacementDirection)
    {
        short x = 0;
        short y = 0;

        if (deplacementDirection == DeplacementDirection.HAUT)
        {
            y = -1;
        }
        else if (deplacementDirection == DeplacementDirection.BAS)
        {
            y = 1;
        }
        else if (deplacementDirection == DeplacementDirection.GAUCHE)
        {
            x = -1;
        }
        else if (deplacementDirection == DeplacementDirection.DROITE)
        {
            x = 1;
        }

        return new Vecteur2D(x, y);
    }

    private boolean grilleCoordonneesValide(Vecteur2D grilleCoordonnees)
    {
        return grilleCoordonnees.x() >= 0 && grilleCoordonnees.y() >= 0 && grilleCoordonnees.x() < grilleNombreLigneColonne && grilleCoordonnees.y() < grilleNombreLigneColonne;
    }

    private boolean peutDeplacerTuiles()
    {
        if (!grilleCoordonneesDisponibles.isEmpty()) //Vérification que la grille n'est pas pleine.
        {
            return true;
        }

        //La grille est pleine.

        Vecteur2D verificationFusionTuilesVecteurDroite = deplacementDirectionVecteur(DeplacementDirection.DROITE);
        Vecteur2D verificationFusionTuilesVecteurBas = deplacementDirectionVecteur(DeplacementDirection.BAS);
        List<Vecteur2D> verificationFusionTuilesVecteurs = Arrays.asList(verificationFusionTuilesVecteurDroite, verificationFusionTuilesVecteurBas);

        //Vérification que des tuiles peuvent fusionner car la grille est pleine avec les tuiles voisines situées à droite et en dessous.
        //A noter que le choix des tuiles voisines, de la façon dont la grille est traversée, de la tuile du départ n'est pas le seul. Il y en a 8.
        //On peut très bien utiliser les autres tuiles voisines à gauche et en haut, inverser le sens du traversement actuel et comme tuile de départ utiliser la dernière traversée actuellement.
        for (short y = 0; y < grilleNombreLigneColonne; y++) //Départ à la ligne la plus haute. Une fois que le traversement de la ligne est fini, on passe à la ligne en dessous.
        {
            for (short x = 0; x < grilleNombreLigneColonne; x++) //Traversement des tuiles de la ligne de gauche à droite.
            {
                Vecteur2D coordonneesTuile = new Vecteur2D(x, y);
                int tuileValeur = grilleTuiles[y][x];

                for (Vecteur2D verificationFusionTuilesVecteur : verificationFusionTuilesVecteurs)
                {
                    short coordonneesAVerifierX = (short) (coordonneesTuile.x() + verificationFusionTuilesVecteur.x());
                    short coordonneesAVerifierY = (short) (coordonneesTuile.y() + verificationFusionTuilesVecteur.y());
                    Vecteur2D coordonneesAVerifier = new Vecteur2D(coordonneesAVerifierX, coordonneesAVerifierY);

                    if (grilleCoordonneesValide(coordonneesAVerifier)) //Bordure atteinte, pas de tuile voisine.
                    {
                        int tuileAVerifierValeur = grilleTuiles[coordonneesAVerifier.y()][coordonneesAVerifier.x()];

                        if (tuileValeur == tuileAVerifierValeur)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public void deplacerTuilesRequete(DeplacementDirection deplacementDirection)
    {
        if (partieEtat != PartieEtat.EN_COURS && partieEtat != PartieEtat.CONTINUEE) //Invalidation de la requête.
        {
            //La requête est invalide.

            return;
        }

        //La requête est valide. La partie est soit en cours soit continuée.

        boolean scoreAChange = false; //Information.

        //Informations.
        Map<Vecteur2D, Vecteur2D> deplacementTuilesSansNonPrioritaires = new LinkedHashMap<>(); //Informations permettant de savoir si des tuiles ont été déplacées.
        Map<Vecteur2D, Integer> fusionTuilesPrioritaires = new LinkedHashMap<>();
        Map<Vecteur2D, Vecteur2D> deplacementFusionTuilesNonPrioritaires = new LinkedHashMap<>(); //Informations permettant de savoir si des tuiles ont été déplacées.

        Vecteur2D deplacementDirectionVecteur = deplacementDirectionVecteur(deplacementDirection);

        //Basé sur un déplacement vers la gauche.
        //A noter que la raison du choix de la base n'a pas d'importance.
        boolean inverserXY = deplacementDirection == DeplacementDirection.HAUT || deplacementDirection == DeplacementDirection.BAS;
        boolean inverserSens = deplacementDirection == DeplacementDirection.BAS || deplacementDirection == DeplacementDirection.DROITE;

        grilleCoordonneesDisponibles.clear(); //Enlèvement de toutes les cases disponibles afin de les redéterminer.

        //Coordonnées de la tuile traversée.
        short y = 0;
        short x = 0;

        for (short s = 0; s < grilleNombreLigneColonne; s++) //Départ à la ligne la plus haute. Une fois que le traversement de la ligne est fini, on passe à la ligne en dessous.
        {
            //Nouvelle variable à chaque changement de ligne ou colonne.
            Vecteur2D coordonneesNouvelles; //La case où la tuile va fusionner ou se déplacer si possible. Initialisée à la case dans la ligne ou colonne, la plus proche du bord en fonction de la direction.

            if (!inverserXY)
            {
                //Déplacement vers la gauche ou vers la droite.

                y = s; //Départ à la ligne la plus haute. Une fois que le traversement de la ligne est fini, on passe à la ligne en dessous.
                if (!inverserSens)
                {
                    //Déplacement vers la gauche.

                    coordonneesNouvelles = new Vecteur2D((short) 0, y); //Initialisée à la case la plus à gauche de la ligne.
                }
                else
                {
                    //Déplacement vers la droite.

                    coordonneesNouvelles = new Vecteur2D((short) (grilleNombreLigneColonne - 1), y); //Initialisée à la case la plus à droite de la ligne.
                }
            }
            else
            {
                //Déplacement vers le haut ou vers le bas.

                x = s; //Départ à la colonne la plus à gauche. Une fois que le traversement de la ligne est fini, on passe à la colonne de droite.
                if (!inverserSens)
                {
                    //Déplacement vers le haut.

                    coordonneesNouvelles = new Vecteur2D(x, (short) 0); //Initialisée à la case la plus en haut de la colonne.
                }
                else
                {
                    //Déplacement vers le bas.

                    coordonneesNouvelles = new Vecteur2D(x, (short) (grilleNombreLigneColonne - 1)); //Initialisée à la case la plus en bas de la colonne.
                }
            }

            //Nouvelles variables à chaque changement de ligne ou colonne.
            Integer coordonneesNouvellesPrecedentesTuileValeur = null;
            boolean coordonneesNouvellesPrecedentesTuilePeutFusionner = true;

            for (short t = 0; t < grilleNombreLigneColonne; t++) //Traversement des tuiles de la ligne de gauche à droite.
            {
                if (!inverserSens)
                {
                    //Déplacement vers la gauche ou vers le haut.

                    if (!inverserXY)
                    {
                        //Déplacement vers la gauche

                        x = t; //Traversement des tuiles de la ligne de gauche à droite.
                    }
                    else
                    {
                        //Déplacement vers le haut.

                        y = t; //Traversement des tuiles de la colonne de haut en bas.
                    }
                }
                else
                {
                    //Déplacement vers la droite ou vers le bas.

                    if (!inverserXY)
                    {
                        //Déplacement vers la droite.

                        x = (short) (grilleNombreLigneColonne - 1 - t); //Traversement des tuiles de la ligne de droite à gauche.
                    }
                    else
                    {
                        //Déplacement vers le bas.

                        y = (short) (grilleNombreLigneColonne - 1 - t); //Traversement des tuiles de la colonne de bas en haut.
                    }
                }

                boolean tuileExiste = grilleTuiles[y][x] != null;
                if (tuileExiste) //Vérification que la case contient une tuile.
                {
                    //La case contient une tuile.

                    Vecteur2D coordonneesTuile = new Vecteur2D(x, y);
                    int tuileValeur = grilleTuiles[y][x];

                    if (coordonneesNouvellesPrecedentesTuileValeur != null && coordonneesNouvellesPrecedentesTuilePeutFusionner && coordonneesNouvellesPrecedentesTuileValeur == tuileValeur) //Vérification qu'une tuile précédente existe, peut fusionner et a la même valeur que la tuile.
                    {
                        //La tuile peut fusionner.

                        short coordonneesNouvellesPrecedentesX = (short) (coordonneesNouvelles.x() + deplacementDirectionVecteur.x());
                        short coordonneesNouvellesPrecedentesY = (short) (coordonneesNouvelles.y() + deplacementDirectionVecteur.y());
                        Vecteur2D coordonneesNouvellesPrecedentes = new Vecteur2D(coordonneesNouvellesPrecedentesX, coordonneesNouvellesPrecedentesY); //Récupération de la case précédente.

                        int coordonneesNouvellesPrecedentesTuileValeurFusion = tuileValeur + coordonneesNouvellesPrecedentesTuileValeur; //Calcul de la nouvelle valeur de la tuile précédente issue de la fusion de cette tuile.

                        grilleTuiles[y][x] = null; //Enlèvement de la tuile de la grille. Elle n'est pas considérée comme tuile précédente pour la prochaine tuile.
                        grilleTuiles[coordonneesNouvellesPrecedentes.y()][coordonneesNouvellesPrecedentes.x()] = coordonneesNouvellesPrecedentesTuileValeurFusion; //Mise à jour de la valeur de la tuile précédente dans la grille. Elle est considérée encore comme tuile précédente pour la prochaine tuile.

                        //Informations.
                        short deplacementTuileX = (short) (coordonneesNouvellesPrecedentes.x() - coordonneesTuile.x());
                        short deplacementTuileY = (short) (coordonneesNouvellesPrecedentes.y() - coordonneesTuile.y());
                        Vecteur2D deplacementTuile = new Vecteur2D(deplacementTuileX, deplacementTuileY);
                        deplacementFusionTuilesNonPrioritaires.put(coordonneesTuile, deplacementTuile);

                        fusionTuilesPrioritaires.put(coordonneesNouvellesPrecedentes, coordonneesNouvellesPrecedentesTuileValeurFusion); //Informations.

                        partieScore += coordonneesNouvellesPrecedentesTuileValeurFusion; //Incrémentation du score de la nouvelle valeur de la tuile précédente.
                        scoreAChange = true; //Information.

                        //Vérification de la création de la tuile de la victoire suite à la fusion si la partie est en cours.
                        if (partieEtat == PartieEtat.EN_COURS && coordonneesNouvellesPrecedentesTuileValeurFusion == victoireTuileValeur)
                        {
                            //Une tuile de la victoire a été créée.

                            partieEtat = PartieEtat.GAGNEE; //Mise à jour de l'état de la partie.
                        }

                        coordonneesNouvellesPrecedentesTuileValeur = coordonneesNouvellesPrecedentesTuileValeurFusion; //Mise à jour de la variable pour la prochaine tuile pour vérifier la possibilité de la fusion avec cette tuile.
                        coordonneesNouvellesPrecedentesTuilePeutFusionner = false; //Mise à jour de la variable pour la prochaine tuile car cette tuile ne peut pas refusionner.
                    }
                    else
                    {
                        //La tuile ne peut pas fusionner.

                        if (!Vecteur2D.memeXY(coordonneesNouvelles, coordonneesTuile)) //Vérification que la tuile peut être déplacée.
                        {
                            grilleTuiles[y][x] = null; //Enlèvement de la tuile de la grille aux anciennes coordonnées.
                            grilleTuiles[coordonneesNouvelles.y()][coordonneesNouvelles.x()] = tuileValeur; //Ajout de la tuile dans la grille aux nouvelles coordonnées.

                            //Informations.
                            short deplacementTuileX = (short) (coordonneesNouvelles.x() - coordonneesTuile.x());
                            short deplacementTuileY = (short) (coordonneesNouvelles.y() - coordonneesTuile.y());
                            Vecteur2D deplacementTuile = new Vecteur2D(deplacementTuileX, deplacementTuileY);
                            deplacementTuilesSansNonPrioritaires.put(coordonneesTuile, deplacementTuile);
                        }

                        short coordonneesNouvellesSuivantesX = (short) (coordonneesNouvelles.x() - deplacementDirectionVecteur.x());
                        short coordonneesNouvellesSuivantesY = (short) (coordonneesNouvelles.y() - deplacementDirectionVecteur.y());
                        coordonneesNouvelles = new Vecteur2D(coordonneesNouvellesSuivantesX, coordonneesNouvellesSuivantesY); //Mise à jour des coordonnées nouvelles pour la prochaine case.

                        coordonneesNouvellesPrecedentesTuileValeur = tuileValeur; //Mise à jour de la variable pour la prochaine tuile pour vérifier la possibilité de la fusion avec cette tuile.
                        coordonneesNouvellesPrecedentesTuilePeutFusionner = true; //Réinitialisation de la variable pour la prochaine tuile car cette tuile est fusionnable.
                    }
                }
            }

            //Ajout des cases disponibles restantes de la ligne ou colonne après le déplacement des tuiles de la ligne ou colonne.
            while (grilleCoordonneesValide(coordonneesNouvelles))
            {
                grilleCoordonneesDisponibles.add(coordonneesNouvelles); //Ajout de la case disponible.

                short coordonneesNouvellesSuivantesX = (short) (coordonneesNouvelles.x() - deplacementDirectionVecteur.x());
                short coordonneesNouvellesSuivantesY = (short) (coordonneesNouvelles.y() - deplacementDirectionVecteur.y());
                coordonneesNouvelles = new Vecteur2D(coordonneesNouvellesSuivantesX, coordonneesNouvellesSuivantesY); //Mise à jour des coordonnées nouvelles pour la prochaine case.
            }
        }

        boolean aucunDeplacementTuile = deplacementTuilesSansNonPrioritaires.isEmpty() && deplacementFusionTuilesNonPrioritaires.isEmpty();
        if (aucunDeplacementTuile) //Vérification qu'aucune tuile n'a été déplacée.
        {
            //Aucune tuile n'a été déplacée.

            return;
        }

        //Au moins une tuile a été déplacée.

        Map<Vecteur2D, Integer> nouvellesTuiles = ajouterTuilePondereeAleatoireCoordonneesAleatoiresRequete(deplacementTuilesNombreTuileAjout); //Requête d'ajout des tuiles si au moins une tuile a été déplacée.

        if (partieEtat == PartieEtat.EN_COURS) //Vérification que la partie est toujours en cours, elle n'a pas été gagnée lors d'une fusion de tuiles.
        {
            //Vérification de la création de la tuile de la victoire pendant l'ajout des nouvelles tuiles.
            for (Integer tuileValeur : nouvellesTuiles.values())
            {
                if (tuileValeur == victoireTuileValeur)
                {
                    //Une tuile de la victoire a été créée.

                    partieEtat = PartieEtat.GAGNEE; //Mise à jour de l'état de la partie.
                    break; //Plus besoin de continuer la vérification des autres tuiles. On sort de la boucle.
                }
            }
        }

        if (partieEtat != PartieEtat.GAGNEE && !peutDeplacerTuiles()) //Vérification qu'aucune tuile ne peut être déplacée, si la partie n'a pas été gagnée lors de l'ajout des tuiles ou d'une fusion.
        {
            //La partie n'est pas gagnée et aucune tuile ne peut être déplacée. La partie est perdue.

            partieEtat = PartieEtat.PERDUE; //Mise à jour de l'état de la partie.
        }

        //Exécution des implémentations de l'événement.
        for (JeuEvenement jeuEvenement : jeuEvenements)
        {
            jeuEvenement.miseAJourPartie(deplacementTuilesSansNonPrioritaires, fusionTuilesPrioritaires, deplacementFusionTuilesNonPrioritaires, nouvellesTuiles, partieEtat, scoreAChange, partieScore);
        }
    }

    public void recommencerPartieRequete()
    {
        //La requête est tout le temps valide à n'importe quel état de la partie.

        //Initialisation des variables.
        grilleTuiles = new Integer[grilleNombreLigneColonne][grilleNombreLigneColonne];
        grilleCoordonneesDisponibles = new ArrayList<>();
        //Toutes les coordonnées sont disponibles.
        for (short y = 0; y < grilleNombreLigneColonne; y++)
        {
            for (short x = 0; x < grilleNombreLigneColonne; x++)
            {
                Vecteur2D grilleCoordonnees = new Vecteur2D(x, y);
                grilleCoordonneesDisponibles.add(grilleCoordonnees);
            }
        }
        aleatoireGrilleCoordonnees = new Random();
        partieScore = 0;
        partieEtat = PartieEtat.EN_COURS;

        //Exécution des implémentations de l'événement.
        for (JeuEvenement jeuEvenement : jeuEvenements)
        {
            jeuEvenement.recommencerPartie();
        }

        Map<Vecteur2D, Integer> nouvellesTuiles = ajouterTuilePondereeAleatoireCoordonneesAleatoiresRequete(commencementPartieNombreTuileAjout); //Requête d'ajout des tuiles du commencement de la partie.

        //Vérification de la création de la tuile de la victoire pendant l'ajout des nouvelles tuiles.
        for (Integer tuileValeur : nouvellesTuiles.values())
        {
            if (tuileValeur == victoireTuileValeur)
            {
                //Une tuile de la victoire a été créée.

                partieEtat = PartieEtat.GAGNEE; //Mise à jour de l'état de la partie.
                break; //Plus besoin de continuer la vérification des autres tuiles. On sort de la boucle.
            }
        }

        if (partieEtat != PartieEtat.GAGNEE && !peutDeplacerTuiles()) //Vérification qu'aucune tuile ne peut être déplacée, si la partie n'a pas été gagnée lors de l'ajout des tuiles.
        {
            //La partie n'est pas gagnée et aucune tuile ne peut être déplacée. La partie est perdue.

            partieEtat = PartieEtat.PERDUE; //Mise à jour de l'état de la partie.
        }

        //Exécution des implémentations de l'événement.
        for (JeuEvenement jeuEvenement : jeuEvenements)
        {
            jeuEvenement.miseAJourPartie(null, null, null, nouvellesTuiles, partieEtat, true, partieScore);
        }
    }

    public void continuerPartieGagneeRequete()
    {
        if (partieEtat != PartieEtat.GAGNEE) //Invalidation de la requête.
        {
            //La requête est invalide.

            return;
        }

        //La requête est valide. La partie est gagnée.

        if (!peutDeplacerTuiles()) //Vérification qu'aucune tuile ne peut être déplacée.
        {
            //Aucune tuile ne peut être déplacée.

            recommencerPartieRequete(); //La partie ne peut pas être continuée, recommencement automatique.
        }
        else
        {
            //Au moins une tuile peut être déplacée. La partie gagnée peut être continuée.

            partieEtat = PartieEtat.CONTINUEE; //Mise à jour de l'état de la partie.
            //Exécution des implémentations de l'événement.
            for (JeuEvenement jeuEvenement : jeuEvenements)
            {
                jeuEvenement.continuerPartieGagnee();
            }
        }
    }

}
