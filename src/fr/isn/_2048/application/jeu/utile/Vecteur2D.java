package fr.isn._2048.application.jeu.utile;

public class Vecteur2D
{

    private short x;
    private short y;

    public Vecteur2D(short x, short y)
    {
        this.x = x;
        this.y = y;
    }

    public static boolean memeXY(Vecteur2D premierVecteur, Vecteur2D deuxiemeVecteur)
    {
        if (premierVecteur == null || deuxiemeVecteur == null)
        {
            return false;
        }

        return premierVecteur.x() == deuxiemeVecteur.x() && premierVecteur.y() == deuxiemeVecteur.y();
    }

    public short x()
    {
        return x;
    }

    public short y()
    {
        return y;
    }

}
