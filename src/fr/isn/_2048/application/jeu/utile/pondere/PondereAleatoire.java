package fr.isn._2048.application.jeu.utile.pondere;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PondereAleatoire
{

    private List<PondereObjet> ponderesObjets;
    private Random aleatoire;
    private int poidsTotal;

    public PondereAleatoire()
    {
        ponderesObjets = new ArrayList<>();
        aleatoire = new Random();
        poidsTotal = 0;
    }

    public void ajouterPondereObjet(PondereObjet pondereObjet)
    {
        if (pondereObjet == null || pondereObjet.poids() <= 0 || ponderesObjets.contains(pondereObjet))
        {
            return;
        }

        poidsTotal += pondereObjet.poids(); //Incrémentation du poids total avec le poids de l'objet.
        ponderesObjets.add(pondereObjet);
    }

    public boolean contientPondereObjet(PondereObjet pondereObjet)
    {
        return ponderesObjets.contains(pondereObjet);
    }

    public void retirerPondereObjet(PondereObjet pondereObjet)
    {
        if (pondereObjet == null || pondereObjet.poids() <= 0 || !ponderesObjets.contains(pondereObjet))
        {
            return;
        }

        poidsTotal -= pondereObjet.poids(); //Décrémentation du poids total avec le poids de l'objet.
        ponderesObjets.remove(pondereObjet);
    }

    /*
    L'algorithme modélise chaque objet dans un intervalle.
    Le début de l'intervalle de l'objet est le poids total des objets précédents dans la liste + 1.
    La fin de l'intervalle de l'objet est le poids total des objets précédents dans la liste + le poids de l'objet.
    La valeur aléatoire est comprise entre 0 et le poids total - 1.
    Le fait de décrémenter la valeur aléatoire par le poids de chaque objet dans l'ordre de la liste jusqu'à qu'elle soit strictement inférieure à 0,
    permet de déterminer l'intervalle où la (valeur aléatoire + 1) est comprise et ainsi l'objet aléatoire.

    Exemple: 1 a un poids de 10, 2 a un poids de 20 et 3 a un poids de 30.
    1 est compris entre 1 et 10, 2 entre 11 et 30, 3 entre 31 et 60.
    La valeur aléatoire est 37.
    37 - 10 = 27
    27 - 20 = 7
    7 - 30 = -23
    -23 < 0
    Donc 3 est l'objet aléatoire.
    */
    public Object aleatoireObjet()
    {
        int valeurAleatoire = aleatoire.nextInt(poidsTotal); //Renvoie d'un entier aléatoire entre 0 inclus et le poids total exclu.

        for (PondereObjet pondereObjet : ponderesObjets)
        {
            valeurAleatoire -= pondereObjet.poids();
            if (valeurAleatoire < 0)
            {
                return pondereObjet.objet();
            }
        }

        return null;
    }

}
