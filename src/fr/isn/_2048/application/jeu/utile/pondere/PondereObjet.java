package fr.isn._2048.application.jeu.utile.pondere;

public class PondereObjet
{

    private int poids;
    private Object objet;

    public PondereObjet(int poids, Object objet)
    {
        this.poids = poids;
        this.objet = objet;
    }

    public int poids()
    {
        return poids;
    }

    public Object objet()
    {
        return objet;
    }

}
