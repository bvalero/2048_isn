package fr.isn._2048.application.jeu;

import fr.isn._2048.application.jeu.utile.Vecteur2D;

import java.util.Map;

public interface JeuEvenement
{

    /*
    Lors d'une fusion entre deux tuiles, la tuile qui était la plus éloignée du bord en fonction de la direction est considérée comme non prioritaire, l'autre comme prioritaire.
    La tuile non prioritaire disparaît.
    La tuile prioritaire prend la valeur double.

    Le déplacement des tuiles, sans les tuiles non prioritaires, équivaut aux tuiles qui ne fusionnent pas et qui se déplacent juste. Et à celles qui fusionnent et qui sont prioritaires.

    Les informations du déplacement des tuiles, sans les tuiles non prioritaires, sont les coordonnées sous forme d'un vecteur des tuiles avant le déplacement, et les déplacements sous forme d'un vecteur.
    Les informations de la fusion des tuiles prioritaires sont les coordonnées sous forme d'un vecteur des tuiles après déplacement, et les nouvelles valeurs.
    Les informations du déplacement des tuiles non prioritaires sont les coordonnées sous forme d'un vecteur des tuiles avant le déplacement, et les déplacements sous forme d'un vecteur.
    Les informations des nouvelles tuiles sont les coordonnées sous forme d'un vecteur des tuiles et leur valeur.

    L'interface Map représente un système de clé-valeur. L'objet HashMap est une implémentation. L'objet LinkedHashMap est une implémentation qui permet de garder l'ordre d'insertion lors de l'itération.
     */
    void miseAJourPartie(Map<Vecteur2D, Vecteur2D> deplacementTuilesSansNonPrioritaires, Map<Vecteur2D, Integer> fusionTuilesPrioritaires, Map<Vecteur2D, Vecteur2D> deplacementFusionTuilesNonPrioritaires, Map<Vecteur2D, Integer> nouvellesTuiles, PartieEtat partieEtat, boolean scoreAChange, int score);

    void recommencerPartie();

    void continuerPartieGagnee();

}
