package fr.isn._2048.application.jeu;

public enum PartieEtat
{

    EN_COURS,
    PERDUE,
    GAGNEE,
    CONTINUEE

}
