package fr.isn._2048.application.jeu;

public enum DeplacementDirection
{

    HAUT,
    BAS,
    GAUCHE,
    DROITE

}
