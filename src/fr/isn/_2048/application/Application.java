package fr.isn._2048.application;

import fr.isn._2048.application.jeu.DeplacementDirection;
import fr.isn._2048.application.jeu.Jeu;
import fr.isn._2048.application.jeu.JeuEvenement;
import fr.isn._2048.application.jeu.PartieEtat;
import fr.isn._2048.application.jeu.utile.Vecteur2D;
import fr.isn._2048.application.utile.SauvegardeFichierInt;
import javafx.animation.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class Application extends javafx.application.Application
{

    private KeyCode deplacerTuilesHautToucheCode;
    private KeyCode deplacerTuilesBasToucheCode;
    private KeyCode deplacerTuilesGaucheToucheCode;
    private KeyCode deplacerTuilesDroiteToucheCode;
    private KeyCode recommencerPartieToucheCode;
    private KeyCode continuerPartieGagneeToucheCode;

    private String titre;

    private int enteteHauteurPixel;
    private Color enteteFondCouleur;

    private String scoreTexte;
    private Color scoreTexteCouleur;
    private Font scoreTextePolice;
    private int scoreBordureGaucheEspacePixel;

    private String meilleurScoreTexte;
    private Color meilleurScoreTexteCouleur;
    private Font meilleurScoreTextePolice;
    private int meilleurScoreBordureDroiteEspacePixel;

    private Color grilleFondCouleur;
    private Color grilleTuileFondCouleur;

    private int tuileTaillePixel;
    private int tuileEspacePixel;
    private Map<Integer, Color> tuileFondCouleurs;
    private Color tuileInconnueFondCouleur;
    private Map<Integer, Color> tuileValeurTexteCouleurs;
    private Color tuileInconnueValeurTexteCouleur;
    private Font tuileValeurTextePolice;
    private int tuileValeurTexteBordureEspacePixel;

    private Duration tuileDeplacementTransitionTranslationDuree;
    private Duration tuileFusionTransitionEchelleDuree;
    private int tuileFusionTransitionEchelleTaillePixelAjout;
    private Duration tuileApparitionTransitionEchelleDuree;

    private String partiePerdueTexte;
    private Color partiePerdueTexteCouleur;
    private Font partiePerdueTextePolice;
    private Color partiePerdueFondCouleur;

    private String partieGagneeTexte;
    private Color partieGagneeTexteCouleur;
    private Font partieGagneeTextePolice;
    private Color partieGagneeFondCouleur;

    private String regagnerFocusTexte;
    private Color regagnerFocusTexteCouleur;
    private Font regagnerFocusTextePolice;
    private Color regagnerFocusFondCouleur;

    private Duration introductionTransitionPauseDuree;

    private int basDePageHauteurPixel;
    private Color basDePageFondCouleur;

    private String basDePageTexte;
    private Color basDePageTexteCouleur;
    private Font basDePageTextePolice;


    private SauvegardeFichierInt meilleurScoreSauvegardeFichierInt;
    private int meilleurScore;

    private Jeu jeu;


    private String licenceTexte;

    @Override
    public void init() throws IOException
    {
        //Initialisation de l'application.

        deplacerTuilesHautToucheCode = KeyCode.UP;
        deplacerTuilesBasToucheCode = KeyCode.DOWN;
        deplacerTuilesGaucheToucheCode = KeyCode.LEFT;
        deplacerTuilesDroiteToucheCode = KeyCode.RIGHT;
        recommencerPartieToucheCode = KeyCode.R;
        continuerPartieGagneeToucheCode = KeyCode.C;

        titre = "2048";

        enteteHauteurPixel = 100;
        enteteFondCouleur = Color.rgb(250, 248, 239);

        scoreTexte = "SCORE\n$score";
        scoreTexteCouleur = Color.rgb(119, 110, 101);
        scoreTextePolice = Font.font(null, FontWeight.BOLD, 25); //Lorsque le paramètre famille est nul, la famille est celle de la police par défaut.
        scoreBordureGaucheEspacePixel = 15;

        meilleurScoreTexte = "MEILLEUR SCORE\n$meilleurScore";
        meilleurScoreTexteCouleur = Color.rgb(119, 110, 101);
        meilleurScoreTextePolice = Font.font(null, FontWeight.BOLD, 25);
        meilleurScoreBordureDroiteEspacePixel = 15;

        grilleFondCouleur = Color.rgb(187, 173, 160);
        grilleTuileFondCouleur = Color.rgb(238, 228, 218, 0.35);

        tuileTaillePixel = 100;
        tuileEspacePixel = 15;
        tuileFondCouleurs = new HashMap<>();
        tuileFondCouleurs.put(2, Color.rgb(238, 228, 218));
        tuileFondCouleurs.put(4, Color.rgb(237, 224, 200));
        tuileFondCouleurs.put(8, Color.rgb(242, 177, 121));
        tuileFondCouleurs.put(16, Color.rgb(245, 149, 99));
        tuileFondCouleurs.put(32, Color.rgb(246, 124, 95));
        tuileFondCouleurs.put(64, Color.rgb(246, 94, 59));
        tuileFondCouleurs.put(128, Color.rgb(237, 207, 114));
        tuileFondCouleurs.put(256, Color.rgb(237, 204, 97));
        tuileFondCouleurs.put(512, Color.rgb(237, 200, 80));
        tuileFondCouleurs.put(1024, Color.rgb(237, 197, 63));
        tuileFondCouleurs.put(2048, Color.rgb(237, 194, 46));
        tuileInconnueFondCouleur = Color.rgb(60, 58, 50);
        tuileValeurTexteCouleurs = new HashMap<>();
        tuileValeurTexteCouleurs.put(2, Color.rgb(119, 110, 101));
        tuileValeurTexteCouleurs.put(4, Color.rgb(119, 110, 101));
        tuileValeurTexteCouleurs.put(8, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(16, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(32, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(64, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(128, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(256, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(512, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(1024, Color.rgb(249, 246, 242));
        tuileValeurTexteCouleurs.put(2048, Color.rgb(249, 246, 242));
        tuileInconnueValeurTexteCouleur = Color.rgb(249, 246, 242);
        tuileValeurTextePolice = Font.font(null, FontWeight.BOLD, 55);
        tuileValeurTexteBordureEspacePixel = 15;

        tuileApparitionTransitionEchelleDuree = Duration.millis(200);
        tuileDeplacementTransitionTranslationDuree = Duration.millis(100);
        tuileFusionTransitionEchelleDuree = Duration.millis(200);
        tuileFusionTransitionEchelleTaillePixelAjout = 10;

        partiePerdueTexte = "Partie perdue!";
        partiePerdueTexteCouleur = Color.rgb(119, 110, 101);
        partiePerdueTextePolice = Font.font(null, FontWeight.BOLD, 60);
        partiePerdueFondCouleur = Color.rgb(238, 228, 218, 0.5);

        partieGagneeTexte = "Partie gagnée!";
        partieGagneeTexteCouleur = Color.rgb(249, 246, 242);
        partieGagneeTextePolice = Font.font(null, FontWeight.BOLD, 60);
        partieGagneeFondCouleur = Color.rgb(237, 194, 46, 0.5);

        regagnerFocusTexte = "Clicker pour regagner le focus.";
        regagnerFocusTexteCouleur = Color.rgb(249, 246, 242);
        regagnerFocusTextePolice = Font.font(null, FontWeight.BOLD, 60);
        regagnerFocusFondCouleur = Color.rgb(0, 0, 0);

        introductionTransitionPauseDuree = Duration.seconds(5);

        basDePageHauteurPixel = 100;
        basDePageFondCouleur = Color.rgb(250, 248, 239);

        basDePageTexte = "TOUCHES DU JEU:\n" +
                "Les flèches servent à déplacer les tuiles.\n" +
                "«R» sert à recommencer la partie ou à passer l'introduction.\n" +
                "«C» sert à continuer la partie gagnée.\n" +
                "RÈGLES DU JEU:\n" +
                "La partie débute avec l'apparition de deux tuiles sur une grille de 4x4.\n" +
                "L'apparition d'une tuile se fait dans une case disponible de manière aléatoire.\n" +
                "La valeur de la tuile apparue est soit 2 (90% de chance) soit 4 (10% de chance).\n" +
                "Lors d'un déplacement, toutes les tuiles sont déplacées dans la même direction jusqu'à collisionner le bord de la grille ou une autre tuile sur leur chemin.\n" +
                "Si deux tuiles, ayant la même valeur, entrent en collision durant le déplacement, elles fusionnent en une nouvelle tuile de valeur double.\n" +
                "La valeur obtenue est ajoutée au score actuel.\n" +
                "Une tuile ne peut pas refusionner lors du déplacement.\n" +
                "Plus la tuile est proche du bord en fonction de la direction, plus elle est prioritaire pour fusionner.\n" +
                "À la fin du déplacement, s'il y a eu du changement, une nouvelle tuile apparaît.\n" +
                "La partie est gagnée lorsque la tuile 2048 apparaît.\n" +
                "Sinon, la partie est terminée lorsqu'aucune tuile ne peut être déplacée.";
        basDePageTexteCouleur = Color.rgb(119, 110, 101);
        basDePageTextePolice = Font.font(null, FontWeight.BOLD, 12);


        meilleurScoreSauvegardeFichierInt = new SauvegardeFichierInt(Paths.get(System.getProperty("user.home"), ".2048", "meilleurScore.dat"));
        meilleurScoreSauvegardeFichierInt.sauvegarderValeur(0, true); //Création du fichier de sauvegarde du meilleur score s'il n'existe pas afin de charger la valeur du meilleur score.
        meilleurScore = meilleurScoreSauvegardeFichierInt.chargerValeur();

        jeu = new Jeu();


        licenceTexte = "The MIT License (MIT)\n" +
                "\n" +
                "Copyright (c) 2014 Gabriele Cirulli\n" +
                "Copyright (c) 2019 Benjamin Valero\n" +
                "Copyright (c) 2019 Arthur Proust\n" +
                "Copyright (c) 2019 Mouslim Otmani\n" +
                "\n" +
                "Permission is hereby granted, free of charge, to any person obtaining a copy\n" +
                "of this software and associated documentation files (the \"Software\"), to deal\n" +
                "in the Software without restriction, including without limitation the rights\n" +
                "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n" +
                "copies of the Software, and to permit persons to whom the Software is\n" +
                "furnished to do so, subject to the following conditions:\n" +
                "\n" +
                "The above copyright notice and this permission notice shall be included in\n" +
                "all copies or substantial portions of the Software.\n" +
                "\n" +
                "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n" +
                "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n" +
                "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n" +
                "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n" +
                "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n" +
                "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN\n" +
                "THE SOFTWARE.";
    }

    @Override
    public void stop() throws IOException
    {
        //Arrêt de l'application.

        meilleurScoreSauvegardeFichierInt.sauvegarderValeur(meilleurScore, false); //Création du fichier de sauvegarde du meilleur score s'il n'existe pas sinon écrasement du fichier.
    }

    @Override
    public void start(Stage theatre)
    {
        //Démarrage de l'application.

        //Création de l'alerte de la licence.
        Alert licenceAlerte = new Alert(Alert.AlertType.NONE);
        licenceAlerte.setTitle("Licence");
        licenceAlerte.getButtonTypes().add(ButtonType.OK);

        //Création du texte de l'alerte de la licence.
        Text licenceAlerteTexte = new Text();
        licenceAlerteTexte.setText(licenceTexte);
        licenceAlerte.getDialogPane().setContent(licenceAlerteTexte); //Mise du contenu du panneau de dialogue avec le texte de la licence.

        Stage theatreAlerte = (Stage) licenceAlerte.getDialogPane().getScene().getWindow(); //Récupération du panneau de dialogue, puis de la scène, puis du théâtre, afin d'ajouter les icônes.

        //Ajout des icônes de l'alerte de la licence.
        List<Integer> iconeTaillesPixelSupporteesAlerte = Arrays.asList(16, 24, 32, 48, 64);
        for (Integer iconeTaillePixelSupporteeAlerte : iconeTaillesPixelSupporteesAlerte)
        {
            String iconeURL = getClass().getClassLoader().getResource("icone-" + iconeTaillePixelSupporteeAlerte + "x" + iconeTaillePixelSupporteeAlerte + ".png").toExternalForm(); //Récupération de la classe, puis du chargeur de la classe, puis de l'URL de la ressource, puis de la chaîne de caractères représentant l'URL.
            Image iconeImage = new Image(iconeURL);
            theatreAlerte.getIcons().add(iconeImage);
        }

        licenceAlerte.showAndWait(); //Affichage de l'alerte et attente de la fermeture de la fenêtre.

        //Calcul de la largeur de la grille en pixel.
        int grilleLargeurPixel = 0;
        for (short s = 1; s <= jeu.grilleNombreLigneColonne(); s++)
        {
            grilleLargeurPixel += tuileTaillePixel;

            if (s != jeu.grilleNombreLigneColonne()) //Vérification que ce n'est pas le dernier tour de la boucle afin d'ajouter l'espace pour le prochain tour.
            {
                grilleLargeurPixel += tuileEspacePixel;
            }
        }
        grilleLargeurPixel += 2 * tuileEspacePixel; //Ajout des bordures de la grille.

        //Création d'une boîte verticale comme base.
        VBox baseBoiteVerticale = new VBox();
        baseBoiteVerticale.setAlignment(Pos.CENTER);
        baseBoiteVerticale.setFillWidth(false); //Pas de remplissage de largeur.

        //Création du panneau bordure de l'entête.
        BorderPane entetePanneauBordure = new BorderPane();
        entetePanneauBordure.setPrefSize(grilleLargeurPixel, enteteHauteurPixel);
        entetePanneauBordure.setBackground(new Background(new BackgroundFill(enteteFondCouleur, CornerRadii.EMPTY, Insets.EMPTY))); //Remplissage du fond sans coins arrondis et sans marges.
        baseBoiteVerticale.getChildren().add(entetePanneauBordure);

        //Création du texte du score.
        Text scoreTexte = new Text();
        scoreTexte.setText(this.scoreTexte.replace("$score", ""));
        scoreTexte.setFill(scoreTexteCouleur);
        scoreTexte.setFont(scoreTextePolice);
        scoreTexte.setTextAlignment(TextAlignment.CENTER);
        scoreTexte.setBoundsType(TextBoundsType.VISUAL); //Bordures visuelles, pas d'espace entre le texte et les bordures.
        BorderPane.setAlignment(scoreTexte, Pos.CENTER);
        BorderPane.setMargin(scoreTexte, new Insets(0, 0, 0, scoreBordureGaucheEspacePixel));
        entetePanneauBordure.setLeft(scoreTexte);

        //Création du texte du meilleur score.
        Text meilleurScoreTexte = new Text();
        meilleurScoreTexte.setText(this.meilleurScoreTexte.replace("$meilleurScore", String.valueOf(meilleurScore)));
        meilleurScoreTexte.setFill(meilleurScoreTexteCouleur);
        meilleurScoreTexte.setFont(meilleurScoreTextePolice);
        meilleurScoreTexte.setTextAlignment(TextAlignment.CENTER);
        meilleurScoreTexte.setBoundsType(TextBoundsType.VISUAL);
        BorderPane.setAlignment(meilleurScoreTexte, Pos.CENTER);
        BorderPane.setMargin(meilleurScoreTexte, new Insets(0, meilleurScoreBordureDroiteEspacePixel, 0, 0));
        entetePanneauBordure.setRight(meilleurScoreTexte);

        //Création du panneau pile du jeu.
        StackPane jeuPanneauPile = new StackPane();
        jeuPanneauPile.setPrefSize(grilleLargeurPixel, grilleLargeurPixel); //Taille du panneau pile, tous les éléments ajoutés au panneau auront la même taille s'ils sont redimensionnable, et s'ils n'ont pas de dimensions maximales inférieures.
        baseBoiteVerticale.getChildren().add(jeuPanneauPile);

        //Création du panneau de la grille du jeu.
        Pane jeuGrillePanneau = new Pane();
        jeuGrillePanneau.setBackground(new Background(new BackgroundFill(grilleFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
        //Création des panneaux des cases de la grille.
        for (short x = 0; x < jeu.grilleNombreLigneColonne(); x++)
        {
            for (short y = 0; y < jeu.grilleNombreLigneColonne(); y++)
            {
                Pane grilleCasePanneau = new Pane();
                grilleCasePanneau.setPrefSize(tuileTaillePixel, tuileTaillePixel);
                grilleCasePanneau.setBackground(new Background(new BackgroundFill(grilleTuileFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
                grilleCasePanneau.setTranslateX(tuileEspacePixel + x * (tuileTaillePixel + tuileEspacePixel)); //Calcul permettant de trouver l'abscisse du pixel du coin en haut à gauche du panneau représentant la case de la tuile, en fonction de l'index de la colonne de la grille.
                grilleCasePanneau.setTranslateY(tuileEspacePixel + y * (tuileTaillePixel + tuileEspacePixel));
                jeuGrillePanneau.getChildren().add(grilleCasePanneau);
            }
        }
        jeuPanneauPile.getChildren().add(jeuGrillePanneau);

        //Création du panneau contenant les panneaux des tuiles.
        Pane jeuTuilesPanneau = new Pane();
        jeuPanneauPile.getChildren().add(jeuTuilesPanneau);

        //Création du panneau bordure de la défaite de la partie.
        BorderPane jeuPartiePerduePanneauBordure = new BorderPane();
        jeuPartiePerduePanneauBordure.setBackground(new Background(new BackgroundFill(partiePerdueFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
        jeuPartiePerduePanneauBordure.setVisible(false);
        jeuPanneauPile.getChildren().add(jeuPartiePerduePanneauBordure);

        //Création du texte de la défaite de la partie.
        Text partiePerdueTexte = new Text();
        partiePerdueTexte.setText(this.partiePerdueTexte);
        partiePerdueTexte.setFill(partiePerdueTexteCouleur);
        partiePerdueTexte.setFont(partiePerdueTextePolice);
        partiePerdueTexte.setTextAlignment(TextAlignment.CENTER);
        partiePerdueTexte.setWrappingWidth(grilleLargeurPixel); //Largeur maximale du texte en pixel sur une seule ligne, si la largeur est dépassée, la suite du texte sera sur la prochaine ligne.
        partiePerdueTexte.setBoundsType(TextBoundsType.VISUAL);
        jeuPartiePerduePanneauBordure.setCenter(partiePerdueTexte);

        //Création du panneau bordure de la victoire de la partie.
        BorderPane jeuPartieGagneePanneauBordure = new BorderPane();
        jeuPartieGagneePanneauBordure.setBackground(new Background(new BackgroundFill(partieGagneeFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
        jeuPartieGagneePanneauBordure.setVisible(false);
        jeuPanneauPile.getChildren().add(jeuPartieGagneePanneauBordure);

        //Création du texte de la victoire de la partie.
        Text partieGagneeTexte = new Text();
        partieGagneeTexte.setText(this.partieGagneeTexte);
        partieGagneeTexte.setFill(partieGagneeTexteCouleur);
        partieGagneeTexte.setFont(partieGagneeTextePolice);
        partieGagneeTexte.setTextAlignment(TextAlignment.CENTER);
        partieGagneeTexte.setWrappingWidth(grilleLargeurPixel);
        partieGagneeTexte.setBoundsType(TextBoundsType.VISUAL);
        jeuPartieGagneePanneauBordure.setCenter(partieGagneeTexte);

        //Création du panneau bordure pour regagner le focus.
        BorderPane jeuRegagnerFocusPanneauBordure = new BorderPane();
        jeuRegagnerFocusPanneauBordure.setBackground(new Background(new BackgroundFill(regagnerFocusFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
        jeuRegagnerFocusPanneauBordure.setVisible(false);
        jeuPanneauPile.getChildren().add(jeuRegagnerFocusPanneauBordure);

        //Création du texte pour regagner le focus.
        Text regagnerFocusTexte = new Text();
        regagnerFocusTexte.setText(this.regagnerFocusTexte);
        regagnerFocusTexte.setFill(regagnerFocusTexteCouleur);
        regagnerFocusTexte.setFont(regagnerFocusTextePolice);
        regagnerFocusTexte.setTextAlignment(TextAlignment.CENTER);
        regagnerFocusTexte.setWrappingWidth(grilleLargeurPixel);
        regagnerFocusTexte.setBoundsType(TextBoundsType.VISUAL);
        jeuRegagnerFocusPanneauBordure.setCenter(regagnerFocusTexte);

        BorderPane jeuIntroductionTuilePanneauBordure = initialiserTuilePanneauBordure(); //Création du panneau bordure de la tuile en introduction.
        miseAJourTuilePanneauBordure(jeuIntroductionTuilePanneauBordure, jeu.victoireTuileValeur()); //Mise à jour du panneau bordure de la tuile en introduction avec la valeur de la victoire.
        double jeuIntroductionTuileValeurTexteEchelle = (double) grilleLargeurPixel / tuileTaillePixel; //Calcul permettant de trouver l'échelle afin de redimensionner le texte au centre du panneau représentant la tuile.
        jeuIntroductionTuilePanneauBordure.getCenter().setScaleX(jeuIntroductionTuileValeurTexteEchelle);
        jeuIntroductionTuilePanneauBordure.getCenter().setScaleY(jeuIntroductionTuileValeurTexteEchelle);
        jeuPanneauPile.getChildren().add(jeuIntroductionTuilePanneauBordure);

        //Création de la transition pause de l'introduction.
        PauseTransition introductionTransitionPause = new PauseTransition(introductionTransitionPauseDuree);
        //Commencement de la partie à la fin de la transition pause de l'introduction.
        introductionTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent evenement)
            {
                jeu.recommencerPartieRequete();
            }

        });
        //Cacher le panneau bordure de la tuile de l'introduction lorsque la transition pause de l'introduction est arrêtée.
        introductionTransitionPause.statusProperty().addListener(new ChangeListener<Animation.Status>()
        {

            @Override
            public void changed(ObservableValue<? extends Animation.Status> valeurObservable, Animation.Status ancienneValeur, Animation.Status nouvelleValeur)
            {
                if (nouvelleValeur == Animation.Status.STOPPED)
                {
                    jeuIntroductionTuilePanneauBordure.setVisible(false);
                }
            }

        });


        //Création du panneau de défilement du bas de page.
        ScrollPane basDePagePanneauDefilement = new ScrollPane();
        basDePagePanneauDefilement.setPrefSize(grilleLargeurPixel, basDePageHauteurPixel);
        baseBoiteVerticale.getChildren().add(basDePagePanneauDefilement);

        //Création du panneau bordure du bas de page.
        BorderPane basDePagePanneauBordure = new BorderPane();
        basDePagePanneauBordure.setBackground(new Background(new BackgroundFill(basDePageFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));
        basDePagePanneauDefilement.setContent(basDePagePanneauBordure);

        //Création du texte du bas de page.
        Text basDePageTexte = new Text();
        basDePageTexte.setText(this.basDePageTexte);
        basDePageTexte.setFill(basDePageTexteCouleur);
        basDePageTexte.setFont(basDePageTextePolice);
        basDePageTexte.setTextAlignment(TextAlignment.CENTER);
        basDePagePanneauBordure.setCenter(basDePageTexte);

        Scene scene = new Scene(baseBoiteVerticale); //Création de la scène avec comme base la boîte verticale.

        //Ajout des icônes dans le théâtre.
        List<Integer> iconeTaillesPixelSupportees = Arrays.asList(16, 24, 32, 48, 64);
        for (Integer iconeTaillePixelSupportee : iconeTaillesPixelSupportees)
        {
            String iconeURL = getClass().getClassLoader().getResource("icone-" + iconeTaillePixelSupportee + "x" + iconeTaillePixelSupportee + ".png").toExternalForm();
            Image iconeImage = new Image(iconeURL);
            theatre.getIcons().add(iconeImage);
        }

        theatre.setTitle(titre);
        theatre.setResizable(false); //Pas redimensionnable.
        theatre.setScene(scene); //Mise de la scène dans le théâtre.
        theatre.sizeToScene(); //Taille du théâtre en fonction de la scène. La taille de la scène est en fonction de la base boîte verticale. La taille de la base boîte verticale est en fonction des éléments ajoutés (entête, grille, bas de page).
        theatre.show();

        //Pas de défilement horizontale pour le texte du bas de page.
        Bounds fenetreDeVisualisationBornes = basDePagePanneauDefilement.getViewportBounds(); //Important d'appeler cette méthode après l'affichage du théâtre sinon les valeurs ne seront pas initialisées.
        double fenetreDeVisualisationLargeurPixel = fenetreDeVisualisationBornes.getWidth();
        basDePageTexte.setWrappingWidth(fenetreDeVisualisationLargeurPixel);

        jeuPanneauPile.requestFocus(); //Demande du focus du panneau pile du jeu.
        //Demande du focus du panneau pile du jeu lorsqu'on clic dessus.
        jeuPanneauPile.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>()
                {

                    @Override
                    public void handle(MouseEvent evenement)
                    {
                        jeuPanneauPile.requestFocus();
                    }

                });
        //Affichage ou camouflage du panneau bordure pour regagner le focus lorsque le panneau pile du jeu perd ou gagne le focus.
        jeuPanneauPile.focusedProperty().addListener(new ChangeListener<Boolean>()
        {

            @Override
            public void changed(ObservableValue<? extends Boolean> valeurObservable, Boolean ancienneValeur, Boolean nouvelleValeur)
            {
                boolean nouvelleValeurInverse = !nouvelleValeur;
                jeuRegagnerFocusPanneauBordure.setVisible(nouvelleValeurInverse);
            }

        });

        jeu.ajouterJeuEvenement(new JeuEvenement()
        {

            private BorderPane[][] jeuGrilleTuiles; //Tableau représentant la grille afin d'y placer les panneaux bordure des tuiles.

            private SequentialTransition miseAJourPartieTransitionSequentielle;

            @Override
            public void miseAJourPartie(Map<Vecteur2D, Vecteur2D> deplacementTuilesSansNonPrioritaires, Map<Vecteur2D, Integer> fusionTuilesPrioritaires, Map<Vecteur2D, Vecteur2D> deplacementFusionTuilesNonPrioritaires, Map<Vecteur2D, Integer> nouvellesTuiles, PartieEtat partieEtat, boolean scoreAChange, int score)
            {
                //Arrêt de la transition séquentielle de la mise à jour de la partie et enlèvement des transitions ajoutées.
                miseAJourPartieTransitionSequentielle.stop();
                miseAJourPartieTransitionSequentielle.getChildren().clear();

                //Traqueurs.
                List<BorderPane> deplacementFusionTuilesNonPrioritairesTraqueur = new ArrayList<>();
                Map<BorderPane, Vecteur2D> deplacementTuilesSansNonPrioritairesTraqueur = new LinkedHashMap<>();
                Map<BorderPane, Integer> fusionTuilesPrioritairesTraqueur = new LinkedHashMap<>();
                List<BorderPane> fusionTuilesPrioritairesSecondTraqueur = new ArrayList<>();
                List<BorderPane> apparitionTuilesTraqueur = new ArrayList<>();
                List<BorderPane> apparitionTuilesSecondTraqueur = new ArrayList<>();

                ParallelTransition fusionTuilesNonPrioritairesTransitionParallele = new ParallelTransition();
                ParallelTransition deplacementTuilesTransitionParallele = new ParallelTransition();
                if (deplacementFusionTuilesNonPrioritaires != null)
                {
                    for (Map.Entry<Vecteur2D, Vecteur2D> entree : deplacementFusionTuilesNonPrioritaires.entrySet())
                    {
                        Vecteur2D coordonneesTuile = entree.getKey();
                        Vecteur2D deplacementTuile = entree.getValue();

                        short coordonneesNouvellesX = (short) (coordonneesTuile.x() + deplacementTuile.x());
                        short coordonneesNouvellesY = (short) (coordonneesTuile.y() + deplacementTuile.y());
                        Vecteur2D coordonneesNouvelles = new Vecteur2D(coordonneesNouvellesX, coordonneesNouvellesY);

                        BorderPane tuilePanneauBordure = jeuGrilleTuiles[coordonneesTuile.y()][coordonneesTuile.x()];

                        jeuGrilleTuiles[coordonneesTuile.y()][coordonneesTuile.x()] = null;

                        PauseTransition fusionTuileNonPrioritaireTransitionPause = new PauseTransition(Duration.ONE);
                        fusionTuileNonPrioritaireTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                        {

                            @Override
                            public void handle(ActionEvent evenement)
                            {
                                tuilePanneauBordure.toBack(); //Mise en arrière-plan de la tuile non prioritaire.
                            }

                        });
                        fusionTuilesNonPrioritairesTransitionParallele.getChildren().add(fusionTuileNonPrioritaireTransitionPause);

                        //Déplacement des tuiles non prioritaire.
                        deplacementFusionTuilesNonPrioritairesTraqueur.add(tuilePanneauBordure);
                        TranslateTransition deplacementFusionTuileNonPrioritaireTransitionTranslation = new TranslateTransition(tuileDeplacementTransitionTranslationDuree, tuilePanneauBordure);
                        deplacementFusionTuileNonPrioritaireTransitionTranslation.setFromX(tuileEspacePixel + coordonneesTuile.x() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementFusionTuileNonPrioritaireTransitionTranslation.setFromY(tuileEspacePixel + coordonneesTuile.y() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementFusionTuileNonPrioritaireTransitionTranslation.setToX(tuileEspacePixel + coordonneesNouvelles.x() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementFusionTuileNonPrioritaireTransitionTranslation.setToY(tuileEspacePixel + coordonneesNouvelles.y() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementFusionTuileNonPrioritaireTransitionTranslation.setOnFinished(new EventHandler<ActionEvent>()
                        {

                            @Override
                            public void handle(ActionEvent evenement)
                            {
                                jeuTuilesPanneau.getChildren().remove(tuilePanneauBordure); //Enlèvement du panneau de la tuile non prioritaire à la fin de la transition.
                                deplacementFusionTuilesNonPrioritairesTraqueur.remove(tuilePanneauBordure);
                            }

                        });
                        deplacementTuilesTransitionParallele.getChildren().add(deplacementFusionTuileNonPrioritaireTransitionTranslation);
                    }
                }
                miseAJourPartieTransitionSequentielle.getChildren().add(fusionTuilesNonPrioritairesTransitionParallele);

                if (deplacementTuilesSansNonPrioritaires != null)
                {
                    for (Map.Entry<Vecteur2D, Vecteur2D> entree : deplacementTuilesSansNonPrioritaires.entrySet())
                    {
                        Vecteur2D coordonneesTuile = entree.getKey();
                        Vecteur2D deplacementTuile = entree.getValue();

                        short coordonneesNouvellesX = (short) (coordonneesTuile.x() + deplacementTuile.x());
                        short coordonneesNouvellesY = (short) (coordonneesTuile.y() + deplacementTuile.y());
                        Vecteur2D coordonneesNouvelles = new Vecteur2D(coordonneesNouvellesX, coordonneesNouvellesY);

                        BorderPane tuilePanneauBordure = jeuGrilleTuiles[coordonneesTuile.y()][coordonneesTuile.x()];

                        jeuGrilleTuiles[coordonneesTuile.y()][coordonneesTuile.x()] = null;
                        jeuGrilleTuiles[coordonneesNouvelles.y()][coordonneesNouvelles.x()] = tuilePanneauBordure;

                        //Déplacement des tuiles sans les tuiles non prioritaires.
                        deplacementTuilesSansNonPrioritairesTraqueur.put(tuilePanneauBordure, coordonneesNouvelles);
                        TranslateTransition deplacementTuileSansNonPrioritaireTransitionTranslation = new TranslateTransition(tuileDeplacementTransitionTranslationDuree, tuilePanneauBordure);
                        deplacementTuileSansNonPrioritaireTransitionTranslation.setFromX(tuileEspacePixel + coordonneesTuile.x() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementTuileSansNonPrioritaireTransitionTranslation.setFromY(tuileEspacePixel + coordonneesTuile.y() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementTuileSansNonPrioritaireTransitionTranslation.setToX(tuileEspacePixel + coordonneesNouvelles.x() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementTuileSansNonPrioritaireTransitionTranslation.setToY(tuileEspacePixel + coordonneesNouvelles.y() * (tuileTaillePixel + tuileEspacePixel));
                        deplacementTuileSansNonPrioritaireTransitionTranslation.setOnFinished(new EventHandler<ActionEvent>()
                        {

                            @Override
                            public void handle(ActionEvent evenement)
                            {
                                deplacementTuilesSansNonPrioritairesTraqueur.remove(tuilePanneauBordure);
                            }

                        });
                        deplacementTuilesTransitionParallele.getChildren().add(deplacementTuileSansNonPrioritaireTransitionTranslation);
                    }
                }
                miseAJourPartieTransitionSequentielle.getChildren().add(deplacementTuilesTransitionParallele);

                ParallelTransition fusionTuilesPrioritairesTransitionParallele = new ParallelTransition();
                ParallelTransition fusionTuilesPrioritairesSecondeTransitionParallele = new ParallelTransition();
                if (fusionTuilesPrioritaires != null)
                {
                    for (Map.Entry<Vecteur2D, Integer> entree : fusionTuilesPrioritaires.entrySet())
                    {
                        Vecteur2D tuileCoordonnees = entree.getKey();
                        int tuileValeur = entree.getValue();

                        BorderPane tuilePanneauBordure = jeuGrilleTuiles[tuileCoordonnees.y()][tuileCoordonnees.x()];

                        //Mise à jour des tuiles.
                        fusionTuilesPrioritairesTraqueur.put(tuilePanneauBordure, tuileValeur);
                        PauseTransition fusionTuilePrioritaireTransitionPause = new PauseTransition(Duration.ONE);
                        fusionTuilePrioritaireTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                        {

                            @Override
                            public void handle(ActionEvent evenement)
                            {
                                miseAJourTuilePanneauBordure(tuilePanneauBordure, tuileValeur);
                                fusionTuilesPrioritairesTraqueur.remove(tuilePanneauBordure);
                            }

                        });
                        fusionTuilesPrioritairesTransitionParallele.getChildren().add(fusionTuilePrioritaireTransitionPause);

                        //Effet de grossissement pour revenir à taille normal suite à la fusion.
                        fusionTuilesPrioritairesSecondTraqueur.add(tuilePanneauBordure);
                        ScaleTransition fusionTuilePrioritaireTransitionEchelle = new ScaleTransition(tuileFusionTransitionEchelleDuree.divide(2), tuilePanneauBordure); //Durée divisée par 2 car il y aura un deuxième cycle où l'animation sera inversée.
                        fusionTuilePrioritaireTransitionEchelle.setFromX(1);
                        fusionTuilePrioritaireTransitionEchelle.setFromY(1);
                        fusionTuilePrioritaireTransitionEchelle.setByX((double) tuileFusionTransitionEchelleTaillePixelAjout / tuileTaillePixel);
                        fusionTuilePrioritaireTransitionEchelle.setByY((double) tuileFusionTransitionEchelleTaillePixelAjout / tuileTaillePixel);
                        fusionTuilePrioritaireTransitionEchelle.setCycleCount(2);
                        fusionTuilePrioritaireTransitionEchelle.setAutoReverse(true);
                        fusionTuilePrioritaireTransitionEchelle.setOnFinished(new EventHandler<ActionEvent>()
                        {

                            @Override
                            public void handle(ActionEvent evenement)
                            {
                                fusionTuilesPrioritairesSecondTraqueur.remove(tuilePanneauBordure);
                            }

                        });
                        fusionTuilesPrioritairesSecondeTransitionParallele.getChildren().add(fusionTuilePrioritaireTransitionEchelle);
                    }
                }
                miseAJourPartieTransitionSequentielle.getChildren().add(fusionTuilesPrioritairesTransitionParallele);
                miseAJourPartieTransitionSequentielle.getChildren().add(fusionTuilesPrioritairesSecondeTransitionParallele);

                ParallelTransition apparitionTuilesTransitionParallele = new ParallelTransition();
                ParallelTransition apparitionTuilesSecondeTransitionParallele = new ParallelTransition();
                for (Map.Entry<Vecteur2D, Integer> entree : nouvellesTuiles.entrySet())
                {
                    Vecteur2D tuileCoordonnees = entree.getKey();
                    int tuileValeur = entree.getValue();

                    BorderPane tuilePanneauBordure = initialiserTuilePanneauBordure();

                    miseAJourTuilePanneauBordure(tuilePanneauBordure, tuileValeur);

                    tuilePanneauBordure.setTranslateX(tuileEspacePixel + tuileCoordonnees.x() * (tuileTaillePixel + tuileEspacePixel));
                    tuilePanneauBordure.setTranslateY(tuileEspacePixel + tuileCoordonnees.y() * (tuileTaillePixel + tuileEspacePixel));
                    tuilePanneauBordure.setScaleX(0);
                    tuilePanneauBordure.setScaleY(0);

                    jeuGrilleTuiles[tuileCoordonnees.y()][tuileCoordonnees.x()] = tuilePanneauBordure;

                    apparitionTuilesTraqueur.add(tuilePanneauBordure);
                    PauseTransition apparitionTuileTransitionPause = new PauseTransition(Duration.ONE);
                    apparitionTuileTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                    {

                        @Override
                        public void handle(ActionEvent evenement)
                        {
                            jeuTuilesPanneau.getChildren().add(tuilePanneauBordure);
                            apparitionTuilesTraqueur.remove(tuilePanneauBordure);
                        }

                    });
                    apparitionTuilesTransitionParallele.getChildren().add(apparitionTuileTransitionPause);

                    //Apparition du panneau bordure de la nouvelle tuile en douceur.
                    apparitionTuilesSecondTraqueur.add(tuilePanneauBordure);
                    ScaleTransition apparitionTuileTransitionEchelle = new ScaleTransition(tuileApparitionTransitionEchelleDuree, tuilePanneauBordure);
                    apparitionTuileTransitionEchelle.setFromX(0);
                    apparitionTuileTransitionEchelle.setFromY(0);
                    apparitionTuileTransitionEchelle.setToX(1);
                    apparitionTuileTransitionEchelle.setToY(1);
                    apparitionTuileTransitionEchelle.setOnFinished(new EventHandler<ActionEvent>()
                    {

                        @Override
                        public void handle(ActionEvent evenement)
                        {
                            apparitionTuilesSecondTraqueur.remove(tuilePanneauBordure);
                        }

                    });
                    apparitionTuilesSecondeTransitionParallele.getChildren().add(apparitionTuileTransitionEchelle);
                }
                miseAJourPartieTransitionSequentielle.getChildren().add(apparitionTuilesTransitionParallele);
                miseAJourPartieTransitionSequentielle.getChildren().add(apparitionTuilesSecondeTransitionParallele);

                PauseTransition partiePerdueGagneeTransitionPause = new PauseTransition(Duration.ONE); //Création de la transition pause de la défaite ou victoire de la partie.
                //Affichage du panneau bordure de défaite de la partie si la partie est perdue ou celui de la victoire si la partie est gagnée.
                partiePerdueGagneeTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                {

                    @Override
                    public void handle(ActionEvent evenement)
                    {
                        if (partieEtat == PartieEtat.PERDUE)
                        {
                            jeuPartiePerduePanneauBordure.setVisible(true);
                        }
                        else if (partieEtat == PartieEtat.GAGNEE)
                        {
                            jeuPartieGagneePanneauBordure.setVisible(true);
                        }
                    }

                });
                miseAJourPartieTransitionSequentielle.getChildren().add(partiePerdueGagneeTransitionPause);

                //Ecouteur lors du changement du statut de la transition séquentielle de la mise à jour de la partie.
                miseAJourPartieTransitionSequentielle.statusProperty().addListener(new ChangeListener<Animation.Status>()
                {

                    @Override
                    public void changed(ObservableValue<? extends Animation.Status> valeurObservable, Animation.Status ancienneValeur, Animation.Status nouvelleValeur)
                    {
                        if (nouvelleValeur == Animation.Status.STOPPED)
                        {
                            //Si l'animation est arrêtée avant sa fin suite à une nouvelle mise à jour, on finit directement cette mise à jour pour la prochaine avec les traqueurs.

                            for (BorderPane tuilePanneauBordure : deplacementFusionTuilesNonPrioritairesTraqueur)
                            {
                                jeuTuilesPanneau.getChildren().remove(tuilePanneauBordure);
                            }

                            for (Map.Entry<BorderPane, Vecteur2D> entree : deplacementTuilesSansNonPrioritairesTraqueur.entrySet())
                            {
                                BorderPane tuilePanneauBordure = entree.getKey();
                                Vecteur2D coordonneesNouvelles = entree.getValue();

                                tuilePanneauBordure.setTranslateX(tuileEspacePixel + coordonneesNouvelles.x() * (tuileTaillePixel + tuileEspacePixel));
                                tuilePanneauBordure.setTranslateY(tuileEspacePixel + coordonneesNouvelles.y() * (tuileTaillePixel + tuileEspacePixel));
                            }

                            for (Map.Entry<BorderPane, Integer> entree : fusionTuilesPrioritairesTraqueur.entrySet())
                            {
                                BorderPane tuilePanneauBordure = entree.getKey();
                                int tuileValeur = entree.getValue();

                                miseAJourTuilePanneauBordure(tuilePanneauBordure, tuileValeur);
                            }

                            for (BorderPane tuilePanneauBordure : fusionTuilesPrioritairesSecondTraqueur)
                            {
                                tuilePanneauBordure.setScaleX(1);
                                tuilePanneauBordure.setScaleY(1);
                            }

                            for (BorderPane tuilePanneauBordure : apparitionTuilesTraqueur)
                            {
                                jeuTuilesPanneau.getChildren().add(tuilePanneauBordure);
                            }

                            for (BorderPane tuilePanneauBordure : apparitionTuilesSecondTraqueur)
                            {
                                tuilePanneauBordure.setScaleX(1);
                                tuilePanneauBordure.setScaleY(1);
                            }

                            if (partieEtat == PartieEtat.PERDUE)
                            {
                                jeuPartiePerduePanneauBordure.setVisible(true);
                            }
                            else if (partieEtat == PartieEtat.GAGNEE)
                            {
                                jeuPartieGagneePanneauBordure.setVisible(true);
                            }

                            valeurObservable.removeListener(this); //Enlèvement de l'instance de l'écouteur propre à cette mise à jour, afin qu'il ne soit pas exécuté pour les prochaines mises à jour.
                        }
                    }

                });

                miseAJourPartieTransitionSequentielle.play(); //Jouer la transition séquentielle de la mise à jour de la partie.

                //Mise à jour du texte du score et du meilleur score.
                if (scoreAChange)
                {
                    scoreTexte.setText(Application.this.scoreTexte.replace("$score", String.valueOf(score)));

                    if (score > meilleurScore)
                    {
                        meilleurScore = score;
                        meilleurScoreTexte.setText(Application.this.meilleurScoreTexte.replace("$meilleurScore", String.valueOf(meilleurScore)));
                    }
                }
            }

            @Override
            public void recommencerPartie()
            {
                if (miseAJourPartieTransitionSequentielle != null)
                {
                    //Arrêt de la transition séquentielle de la mise à jour de la partie et enlèvement des transitions ajoutées.
                    miseAJourPartieTransitionSequentielle.stop();
                    miseAJourPartieTransitionSequentielle.getChildren().clear();
                }

                //Initialisation des variables.
                jeuGrilleTuiles = new BorderPane[jeu.grilleNombreLigneColonne()][jeu.grilleNombreLigneColonne()];
                miseAJourPartieTransitionSequentielle = new SequentialTransition();

                PauseTransition recommencerPartieTransitionPause = new PauseTransition(Duration.ONE); //Création de la transition pause de recommencement de partie.
                recommencerPartieTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                {

                    @Override
                    public void handle(ActionEvent evenement)
                    {
                        //Nettoyage de la partie précédente.
                        jeuPartiePerduePanneauBordure.setVisible(false);
                        jeuPartieGagneePanneauBordure.setVisible(false);

                        jeuTuilesPanneau.getChildren().clear();
                    }

                });

                recommencerPartieTransitionPause.play();
            }

            @Override
            public void continuerPartieGagnee()
            {
                //Arrêt de la transition séquentielle de la mise à jour de la partie et enlèvement des transitions ajoutées.
                miseAJourPartieTransitionSequentielle.stop();
                miseAJourPartieTransitionSequentielle.getChildren().clear();

                //Cacher le panneau bordure de la victoire de la partie.
                PauseTransition continuerPartieGagneeTransitionPause = new PauseTransition(Duration.ONE); //Création de la transition pause de continuation de la partie gagnée.
                continuerPartieGagneeTransitionPause.setOnFinished(new EventHandler<ActionEvent>()
                {

                    @Override
                    public void handle(ActionEvent evenement)
                    {
                        jeuPartieGagneePanneauBordure.setVisible(false);
                    }

                });

                continuerPartieGagneeTransitionPause.play();
            }

        });

        //Evénement d'une touche appuyée sur le panneau pile du jeu.
        jeuPanneauPile.addEventHandler(KeyEvent.KEY_PRESSED,
                new EventHandler<KeyEvent>()
                {

                    @Override
                    public void handle(KeyEvent evenement)
                    {
                        KeyCode codeTouche = evenement.getCode();
                        if (codeTouche == deplacerTuilesHautToucheCode)
                        {
                            jeu.deplacerTuilesRequete(DeplacementDirection.HAUT);
                        }
                        else if (codeTouche == deplacerTuilesBasToucheCode)
                        {
                            jeu.deplacerTuilesRequete(DeplacementDirection.BAS);
                        }
                        else if (codeTouche == deplacerTuilesGaucheToucheCode)
                        {
                            jeu.deplacerTuilesRequete(DeplacementDirection.GAUCHE);
                        }
                        else if (codeTouche == deplacerTuilesDroiteToucheCode)
                        {
                            jeu.deplacerTuilesRequete(DeplacementDirection.DROITE);
                        }
                        else if (codeTouche == recommencerPartieToucheCode)
                        {
                            introductionTransitionPause.stop(); //Arrêt de la transition pause de l'introduction.

                            jeu.recommencerPartieRequete();
                        }
                        else if (codeTouche == continuerPartieGagneeToucheCode)
                        {
                            jeu.continuerPartieGagneeRequete();
                        }
                    }

                });

        introductionTransitionPause.play(); //Jouer de la transition pause de l'introduction.
    }

    private BorderPane initialiserTuilePanneauBordure()
    {
        //Création du panneau bordure de la tuile.
        BorderPane tuilePanneauBordure = new BorderPane();
        tuilePanneauBordure.setPrefSize(tuileTaillePixel, tuileTaillePixel);

        //Création du texte de la valeur de la tuile.
        Text tuileValeurTexte = new Text();
        tuileValeurTexte.setTextAlignment(TextAlignment.CENTER);
        tuileValeurTexte.setBoundsType(TextBoundsType.VISUAL);
        tuilePanneauBordure.setCenter(tuileValeurTexte);

        return tuilePanneauBordure;
    }

    private void miseAJourTuilePanneauBordure(BorderPane tuilePanneauBordure, int tuileValeur)
    {
        //Mise à jour du fond du panneau bordure de la tuile.
        Color tuileFondCouleur;
        boolean tuileConnueFondCouleur = tuileFondCouleurs.containsKey(tuileValeur);
        if (tuileConnueFondCouleur)
        {
            tuileFondCouleur = tuileFondCouleurs.get(tuileValeur);
        }
        else
        {
            tuileFondCouleur = tuileInconnueFondCouleur;
        }
        tuilePanneauBordure.setBackground(new Background(new BackgroundFill(tuileFondCouleur, CornerRadii.EMPTY, Insets.EMPTY)));

        //Mise à jour du texte de la valeur de la tuile.
        Text tuileValeurTexte = (Text) tuilePanneauBordure.getCenter();

        tuileValeurTexte.setText(String.valueOf(tuileValeur));

        Color tuileValeurTexteCouleur;
        boolean tuileConnueValeurTexteCouleur = tuileValeurTexteCouleurs.containsKey(tuileValeur);
        if (tuileConnueValeurTexteCouleur)
        {
            tuileValeurTexteCouleur = tuileValeurTexteCouleurs.get(tuileValeur);
        }
        else
        {
            tuileValeurTexteCouleur = tuileInconnueValeurTexteCouleur;
        }
        tuileValeurTexte.setFill(tuileValeurTexteCouleur);

        //Si le texte dépasse la taille de la tuile, la taille de la police est réduite afin de ne pas dépasser.
        tuileValeurTexte.setFont(tuileValeurTextePolice);
        double tuileValeurTextePoliceTaille = tuileValeurTextePolice.getSize();
        Bounds tuileValeurTexteBornesLocales = tuileValeurTexte.getBoundsInLocal();
        double tuileValeurTexteLargeur = tuileValeurTexteBornesLocales.getWidth();
        double tuileValeurTexteHauteur = tuileValeurTexteBornesLocales.getHeight();
        double plusGrandeTaille = Math.max(tuileValeurTexteLargeur, tuileValeurTexteHauteur);
        double tuileValeurTextePoliceTailleFinale = tuileValeurTextePoliceTaille;
        int tuileValeurTexteTailleMaximale = tuileTaillePixel - 2 * tuileValeurTexteBordureEspacePixel;
        if (plusGrandeTaille > tuileValeurTexteTailleMaximale)
        {
            tuileValeurTextePoliceTailleFinale = tuileValeurTextePoliceTaille * (tuileValeurTexteTailleMaximale / plusGrandeTaille);
        }
        Font tuileValeurTextePoliceFinale = new Font(tuileValeurTextePolice.getName(), tuileValeurTextePoliceTailleFinale);
        tuileValeurTexte.setFont(tuileValeurTextePoliceFinale);
    }

}