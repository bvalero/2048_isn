package fr.isn._2048.application.utile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;

public class SauvegardeFichierInt
{

    private Path fichierChemin;

    public SauvegardeFichierInt(Path fichierChemin)
    {
        this.fichierChemin = fichierChemin;
    }

    public void sauvegarderValeur(int valeur, boolean annulerSiSauvegardeExiste) throws IOException
    {
        if (annulerSiSauvegardeExiste && Files.exists(fichierChemin))
        {
            return;
        }

        ByteBuffer octetMemoireTampon = ByteBuffer.allocate(4); //Allocation d'une mémoire tampon de 4 octets (= 32 bits = taille d'un int).
        octetMemoireTampon.putInt(valeur); //Mise du int dans la mémoire tampon d'octets.
        byte[] octets = octetMemoireTampon.array(); //Récupération du int sous la forme de 4 octets.
        Files.createDirectories(fichierChemin.getParent()); //Création des répertoires du fichier s'ils n'existent pas.
        Files.write(fichierChemin, octets); //Ecriture des 4 octets dans le fichier (création du fichier s'il n'existe pas sinon écrasement du fichier). La méthode peut jeter une exception entrée sortie (IOException).
    }

    public int chargerValeur() throws IOException
    {
        byte[] contenu = Files.readAllBytes(fichierChemin); //Lecture de tous les octets dans le fichier. La méthode peut jeter une exception entrée sortie (IOException).
        ByteBuffer octetMemoireTampon = ByteBuffer.wrap(contenu); //Allocation d'une mémoire tampon d'octets représentant tous les octets du fichier.
        return octetMemoireTampon.getInt(); //Récupération des 4 premiers octets sous la forme d'un int.
    }

}
